package com.guo.sentinel.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.guo.sentinel.service.UserService;

@RestController
@RequestMapping("/user")
public class UserFeignController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert")
    public Map<String,Object> insert(@RequestBody Map<String,Object> user) {
        return userService.insert(user);
    }

    @GetMapping("/{id}")
    public Map<String,Object> getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @GetMapping("/listUsersByIds")
    public Map<String,Object> listUsersByIds(@RequestParam List<Long> ids) {
        return userService.listUsersByIds(ids);
    }

    @GetMapping("/getByUsername")
    public Map<String,Object> getByUsername(@RequestParam String username) {
        return userService.getByUsername(username);
    }

    @PostMapping("/update")
    public Map<String,Object> update(@RequestBody Map<String,Object> user) {
        return userService.update(user);
    }

    @PostMapping("/delete/{id}")
    public Map<String,Object> delete(@PathVariable Long id) {
        return userService.delete(id);
    }

}
