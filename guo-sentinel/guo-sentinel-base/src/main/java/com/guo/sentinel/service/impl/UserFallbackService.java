package com.guo.sentinel.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.guo.sentinel.service.UserService;

@Service
public class UserFallbackService implements UserService {
	
	@Override
	public Map<String, Object> insert(Map<String, Object> user) {
		return this.installFallBack();
	}

	@Override
	public Map<String, Object> getUser(Long id) {
		return this.installFallBack();
	}

	@Override
	public Map<String, Object> listUsersByIds(List<Long> ids) {
		return this.installFallBack();
	}

	@Override
	public Map<String, Object> getByUsername(String username) {
		return this.installFallBack();
	}

	@Override
	public Map<String, Object> update(Map<String, Object> user) {
		return this.installFallBack();
	}

	@Override
	public Map<String, Object> delete(Long id) {
		return this.installFallBack();
	}

	private Map<String, Object> installFallBack() {
		Map<String, Object> r = new HashMap<>();
		r.put("code", 500);
		r.put("msg", "调用失败，服务被降级");
		return r;
	}

}
