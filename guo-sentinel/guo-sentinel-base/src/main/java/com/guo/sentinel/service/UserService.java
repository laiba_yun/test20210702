package com.guo.sentinel.service;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.guo.sentinel.service.impl.UserFallbackService;

@FeignClient(value = "guo-sentinel-provider", fallback = UserFallbackService.class)
public interface UserService {

    @PostMapping("/user/insert")
    Map<String,Object> insert(@RequestBody Map<String,Object> user);

    @GetMapping("/user/{id}")
    Map<String,Object> getUser(@PathVariable(value = "id") Long id);

    @GetMapping("/user/listUsersByIds")
    Map<String,Object> listUsersByIds(@RequestParam(value = "ids") List<Long> ids);

    @GetMapping("/user/getByUsername")
    Map<String,Object> getByUsername(@RequestParam(value = "username") String username);

    @PostMapping("/user/update")
    Map<String,Object> update(@RequestBody Map<String,Object> user);

    @PostMapping("/user/delete/{id}")
    Map<String,Object> delete(@PathVariable(value = "id") Long id);

}
