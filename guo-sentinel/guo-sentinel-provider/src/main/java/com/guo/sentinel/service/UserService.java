package com.guo.sentinel.service;

import java.util.List;
import java.util.Map;

public interface UserService {

    Map<String,Object> insert(Map<String,Object> user);

    Map<String,Object> getUser(Long id);

    Map<String,Object> listUsersByIds(List<Long> ids);

    Map<String,Object> getByUsername(String username);

    Map<String,Object> update(Map<String,Object> user);

    Map<String,Object> delete(Long id);
	
}
