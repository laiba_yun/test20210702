package com.guo.sentinel.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.guo.sentinel.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Override
	public Map<String, Object> insert(Map<String, Object> user) {
		log.info("新增：{}", user);
		return null;
	}

	@Override
	public Map<String, Object> getUser(Long id) {
		log.info("获取：{}", id);
		if(Long.valueOf("1").equals(id)) {
            throw new RuntimeException("remote func is fail");
        }

        Map<String,Object> result = new HashMap<>();
        result.put("reqData",id);
        result.put("code","200");

        return result ;
	}

	@Override
	public Map<String, Object> listUsersByIds(List<Long> ids) {
		log.info("获取集合：{}", ids);
		return null;
	}

	@Override
	public Map<String, Object> getByUsername(String username) {
		log.info("根据用户名获取：{}", username);
		if(1==1) throw new RuntimeException("模拟异常");
		return null;
	}

	@Override
	public Map<String, Object> update(Map<String, Object> user) {
		log.info("更新：{}", user);
		return null;
	}

	@Override
	public Map<String, Object> delete(Long id) {
		log.info("删除：{}", id);
		return null;
	}

}
