package com.guo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class SysUserController {

	@GetMapping(value = "/info/{username}")
    public String getUserInfo(@PathVariable("username") String username) {
		return username;
	}
	
}
