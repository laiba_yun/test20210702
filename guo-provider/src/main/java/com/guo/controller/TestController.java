package com.guo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@GetMapping(value = "/user/info/{username}")
    public Map<String,Object> getUserInfo(@PathVariable("username") String username) {
		Map<String,Object> result = new HashMap<>();
		result.put("a", "1");
		result.put("b", "1");
		result.put("c", "1");
		result.put("d", "1");
		result.put("e", "1");
		result.put("f", "1");
		result.put("username", username);
		
		// feign调用超时，异常处理测试
		try {
			Thread.sleep(1001);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// feign调用异常，异常处理测试
		if (1 == 1 ) {
			throw new RuntimeException("模拟异常");
		}
		
    	return result;
    }
	
}
