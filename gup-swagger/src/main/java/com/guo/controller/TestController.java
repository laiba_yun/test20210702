package com.guo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("测试接口")
@RequestMapping("/swagger")
@RestController
public class TestController {

	@ApiOperation("说嗨")
    @GetMapping(value = "/hi/{username}")
    public String getUserInfo(@PathVariable("username") String username) {
    	return username;
    }
    
}