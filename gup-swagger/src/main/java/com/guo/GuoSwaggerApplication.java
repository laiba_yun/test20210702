package com.guo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuoSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuoSwaggerApplication.class, args);
    }
}


