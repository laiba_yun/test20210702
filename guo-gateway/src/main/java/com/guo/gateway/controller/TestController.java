package com.guo.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gateway")
public class TestController {
	
	@GetMapping(value = "/user/info/{username}")
    public String getUserInfo(@PathVariable("username") String username) {
    	return username;
    }
	
}
