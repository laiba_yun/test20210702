package com.guo.user.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.guo.common.core.constant.UserApiServiceConstant;

@FeignClient(contextId = "remoteUserService", value = UserApiServiceConstant.USER_SERVICE)
public interface UserApiService {
	
	@GetMapping(value = "/user/info/{username}")
	String getUserInfo(@PathVariable("username") String username);

}
