package com.guo.stater.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "gf.hello")
public class HelloProperties {

    private String prefix;
    
    private String suffix;

}
