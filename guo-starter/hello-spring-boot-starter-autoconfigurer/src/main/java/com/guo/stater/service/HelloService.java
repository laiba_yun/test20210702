package com.guo.stater.service;

import com.guo.stater.config.HelloProperties;

import lombok.Data;

@Data
public class HelloService {
	
	private HelloProperties helloProperties;

	public String sayHello(String name ) {
        return helloProperties.getPrefix()+ "-" + name + helloProperties.getSuffix();
    }
	
}
