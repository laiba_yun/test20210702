# 序言

> springboot的强大之处在于把我们常用的场景抽取成一个个stater（场景启动器），我们通过引入springboot为我们提供的这些场景启动器，我们在进行少量的配置就能使用相应的功能。即使是这样，springboot也不可能囊括我们所有的使用场景，往往我们就需要自定义stater，来简化我们对springboot的使用。

# 借鉴@EnableAdminServer

> 我们在使用 springboot admin server的时候，通常做一下两个步骤：
>
> 1、添加依赖
>
> 2、开启注解

## 添加依赖

```xml
		<!-- SpringBoot Actuator -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<!-- SpringBoot Admin -->
		<dependency>
			<groupId>de.codecentric</groupId>
			<artifactId>spring-boot-admin-starter-server</artifactId>
			<version>${spring-boot-admin.version}</version>
		</dependency>
```

> 我们看下这个依赖包
>
> ![image-20210713092341601](C:\Users\guozl\AppData\Roaming\Typora\typora-user-images\image-20210713092341601.png)



spring-boot-admin-server这个pom.xml 第一个依赖就是

```xml
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter</artifactId>
      <version>2.2.7.RELEASE</version>
      <scope>compile</scope>
    </dependency>
```

![image-20210713160726934](C:\Users\guozl\AppData\Roaming\Typora\typora-user-images\image-20210713160726934.png)

> spring-boot-starter 是自定义启动器必须的





> spring-boot-admin-stater-server-2.2.4.jar只是一个pom,并无实际逻辑，继续看spring-boot-admin-server这个依赖，这是spring-bootadmin-stater-server启动器的配置模块
>
> ![image-20210713093433263](C:\Users\guozl\AppData\Roaming\Typora\typora-user-images\image-20210713093433263.png)
>
> SPI自动注入了4个配置，打开一个看看AdminServerAutoConfiguration
>
> ![image-20210713093820689](C:\Users\guozl\AppData\Roaming\Typora\typora-user-images\image-20210713093820689.png)
>
> 



## 开启注解

> 在启动类上添加开启服务的注解
>
> ```java
> @EnableAdminServer
> @SpringBootApplication
> public class MonitorApplication {
> 
>     public static void main(String[] args) {
>         SpringApplication.run(MonitorApplication.class, args);
>     }
> }
> ```
>
> 我们详细看下这个注解
>
> ```java
> @Target(ElementType.TYPE)
> @Retention(RetentionPolicy.RUNTIME)
> @Documented
> // 导入admin服务的标记配置，用于标识admin服务的启用，在后面服务实例化时作为依赖项引入
> @Import(AdminServerMarkerConfiguration.class)
> public @interface EnableAdminServer {
> 
> }
> 
> @Configuration(proxyBeanMethods = false)
> public class AdminServerMarkerConfiguration {
> 
>     // 向spring容器添加bean组件
> 	@Bean
> 	public Marker adminServerMarker() {
> 		return new Marker();
> 	}
> 
> 	public static class Marker {
> 
> 	}
> 
> }
> 
> ```
>
> 

# 总结出一般自定义stater结构

- 启动器stater只是用来做依赖管理，并无实际逻辑
- 需要专门写一个启动器的配置模块
- 需要写一个自定义注解，用于开启服务
- 用的时候只需要引入启动器stater，在启动类上开启服务

# 命名规范

## 官方命名

- 前缀：spring-boot-starter-
- 模式：spring-boot-starter-模块名
- 举例：spring-boot-starter-web、spring-boot-starter-jdbc

## 自定义命名

- 后缀：-spring-boot-starter
- 模式：模块-spring-boot-starter
- 举例：mybatis-spring-boot-starter

# 实现一个自定义stater

## 项目结构

![image-20210713155947835](C:\Users\guozl\AppData\Roaming\Typora\typora-user-images\image-20210713155947835.png)

guo-hello-spring-boot-starter 场景启动器

guo-hello-spring-boot-starter-autoconfigurer 启动器配置模块

hello-spring-boot-starter-test 自定义启动器测试模块



## 实现

### 启动器管理依赖

> guo-hello-spring-boot-starter 添加启动器配置模块

```xml
<dependency>
  		<groupId>com.guo</groupId>
  		<artifactId>
  			guo-hello-spring-boot-starter-autoconfigurer
  		</artifactId>
  		<version>0.0.1-SNAPSHOT</version>
  	</dependency>
```

### 启动器配置模块添加核心启动器

> guo-hello-spring-boot-starter-autoconfigurer添加核心启动器依赖

```xml
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
```

### 添加启动器启用注解

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(GuoHelloServerMarkerConfiguration.class)
public @interface EnableGuoHelloService {

}
```

### 添加服务标记配置

```java
@Configuration(proxyBeanMethods = false)
public class GuoHelloServerMarkerConfiguration {

	@Bean
	public Marker guoHelloServerMarker() {
		return new Marker();
	}

	public static class Marker {

	}
	
}
```

### SPI注入服务

在资源目录下创建目录 META-INF，在目录下创建文件spring.factories，内容如下

```xml
# Auto Configure
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
com.guo.stater.config.GuoHelloServiceAutoConfiguration
```

### 编写服务

```java
@Configuration(proxyBeanMethods = false)
// 启用注解时EnableGuoHelloService注入bean GuoHelloServerMarkerConfiguration.Marker
@ConditionalOnBean(GuoHelloServerMarkerConfiguration.Marker.class)
// 服务依赖的配置
@EnableConfigurationProperties(GuoHelloServerProperties.class)
// 导入其他服务配置
@ImportAutoConfiguration({ GuoHelloServerConfiguration.class })
@AutoConfigureAfter({ WebClientAutoConfiguration.class })
@Lazy(false)
public class GuoHelloServiceAutoConfiguration {
	
	

}

@lombok.Data
@Configuration(proxyBeanMethods = false)
public class GuoHelloServerConfiguration {
	
	@Autowired
	private GuoHelloServerProperties guoHelloServerProperties;
	
	public String sayHi(String name) {
		return name + " " + guoHelloServerProperties.getHi();
	}

}

@lombok.Data
@ConfigurationProperties("spring.boot.guo.hello")
public class GuoHelloServerProperties {
	
	private String hi;
	
}
```

## 常用注解

```java
@Configuration  //指定这个类是一个配置类
@ConditionalOnXXX  //指定条件成立的情况下自动配置类生效
@AutoConfigureOrder  //指定自动配置类的顺序
@Bean  //向容器中添加组件
@ConfigurationProperties  //结合相关xxxProperties来绑定相关的配置
@EnableConfigurationProperties  //让xxxProperties生效加入到容器中

自动配置类要能加载需要将自动配置类，配置在META-INF/spring.factories中
```

# 测试

## 添加依赖

```xml
		<!-- 引入自定义starter -->
		<dependency>
			<groupId>com.guo</groupId>
			<artifactId>hello-spring-boot-starter</artifactId>
			<version>0.0.1-SNAPSHOT</version>
		</dependency>
```



## 启动类上添加注解

```java
// 添加注解开启服务
@EnableGuoHelloService
@SpringBootApplication
public class StaterApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(StaterApplication.class, args);
	}

}
```



## 使用服务

```java

@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;
    
    @Autowired
    private GuoHelloServerConfiguration guoHelloServerConfiguration;

    @GetMapping("/hello/{name}")
    public String hello(@PathVariable(value = "name") String name) {
        return helloService.sayHello( name + " , " );
    }

    @GetMapping("/hi/{name}")
    public String hi(@PathVariable(value = "name") String name) {
    	return guoHelloServerConfiguration.sayHi(name);
    }
    
}
```

