package com.guo.stater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.guo.stater.config.EnableGuoHelloService;

@EnableGuoHelloService
@SpringBootApplication
public class StaterApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(StaterApplication.class, args);
	}

}
