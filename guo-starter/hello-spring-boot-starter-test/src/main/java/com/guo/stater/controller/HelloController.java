package com.guo.stater.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.guo.stater.config.GuoHelloServerConfiguration;
import com.guo.stater.service.HelloService;

@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;
    
    @Autowired
    private GuoHelloServerConfiguration guoHelloServerConfiguration;

    @GetMapping("/hello/{name}")
    public String hello(@PathVariable(value = "name") String name) {
        return helloService.sayHello( name + " , " );
    }

    @GetMapping("/hi/{name}")
    public String hi(@PathVariable(value = "name") String name) {
    	return guoHelloServerConfiguration.sayHi(name);
    }
    
}
