package com.guo.stater.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class GuoHelloServerMarkerConfiguration {

	@Bean
	public Marker guoHelloServerMarker() {
		return new Marker();
	}

	public static class Marker {

	}
	
}
