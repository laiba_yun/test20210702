package com.guo.stater.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration(proxyBeanMethods = false)
@ConditionalOnBean(GuoHelloServerMarkerConfiguration.Marker.class)
@EnableConfigurationProperties(GuoHelloServerProperties.class)
@ImportAutoConfiguration({ GuoHelloServerConfiguration.class })
@AutoConfigureAfter({ WebClientAutoConfiguration.class })
@Lazy(false)
public class GuoHelloServiceAutoConfiguration {
	
	

}
