package com.guo.stater.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@lombok.Data
@Configuration(proxyBeanMethods = false)
public class GuoHelloServerConfiguration {
	
	@Autowired
	private GuoHelloServerProperties guoHelloServerProperties;
	
	public String sayHi(String name) {
		return name + " " + guoHelloServerProperties.getHi();
	}

}
