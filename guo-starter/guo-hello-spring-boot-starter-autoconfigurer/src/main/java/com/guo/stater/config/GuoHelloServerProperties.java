package com.guo.stater.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@lombok.Data
@ConfigurationProperties("spring.boot.guo.hello")
public class GuoHelloServerProperties {
	
	private String hi;
	
}
