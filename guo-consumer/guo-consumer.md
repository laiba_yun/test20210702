#性能优化
##Gzip压缩
- 在guo-gateway项目添加配置

```
server:
  compression:
    # 是否开启压缩，浏览器到consumer的压缩
    enabled: true
    # 配置支持压缩的 MIME TYPE
    mime-types: text/html,text/xml,text/plain,application/xml,application/json
feign:
  compression:
    request:
      # 开启请求压缩，consumer到provider的压缩
      enabled: true
      # 配置压缩支持的 MIME TYPE
      mime-types: text/xml,application/xml,application/json 
      # 配置压缩数据大小的下限
      min-request-size: 2048  
    response:
      # 开启响应压缩
      enabled: true  
```

- 测试查看请求与相应

```
/guo/img/guo-consumer/1.png
```

##Http连接池

##feign异常配置
- 在feign接口消费方guo-consumer添加配置

```
# 开启断路器
feign:
  hystrix:
    enabled: true
```

- FeignClient接口服务加入fallbackFactory，如 RemoteUserService.java
- 添加接口实现异常类，如 RemoteUserFallbackFactory.java

##请求超时
- 在guo-gateway添加配置

```
#全局配置
# 请求处理的超时时间
ribbon:
  ReadTimeout: 10000
  ConnectTimeout: 10000
#局部配置
# guo-xxxx 为需要调用的服务名称
guo-provider:
  ribbon:
    ReadTimeout: 1000
    ConnectTimeout: 1000
guo-consumer:
  ribbon:
    ReadTimeout: 1000
    ConnectTimeout: 1000
```

- 观察结果

```
我这里给通过调用guo-gateway进行转发到guo-consumer,由guo-consumer调用guo-provider的feign接口，设置guo-consumer的读取超时时间为1000，在guo-provider接口实现中线程睡眠10001，这样会导致guo-consumer读取超时。

调用接口结果
/guo/img/guo-consumer/2.png
/guo/img/guo-consumer/3.png
显示超时
```

##请求拦截器

#服务监控
#链路追踪
#熔断和降级
##web全局异常
#分布式文件