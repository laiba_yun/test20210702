package com.guo.remoteservice;

/**
 * 服务名称
 * 
 * @author guo
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "guo-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "guo-provider";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "guo-file";
}
