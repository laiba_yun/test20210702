package com.guo.remoteservice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;

@Component
public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService> {

	@Override
	public RemoteUserService create(Throwable throwable) {
		return r -> {
			Map<String,Object> result = new HashMap<>();
			result.put("msg", throwable.getMessage());
			return result;
		};
	}
	
}
