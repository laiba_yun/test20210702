package com.guo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.guo.remoteservice.RemoteUserService;

@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
    private RemoteUserService remoteUserService;

	@GetMapping(value = "/info/{username}")
    public Map<String,Object> getUserInfo(@PathVariable("username") String username) {
		return remoteUserService.getUserInfo(username);
	}
	
}
