项目参考若依微服务框架
http://doc.ruoyi.vip/ruoyi-cloud/cloud/sentinel.html

#系统结构
~~~
com.guo     
├── guo-ui              // 前端框架 [80]
├── guo-gateway         // 网关模块 [8080]
├── guo-auth            // 认证中心 [9200]
├── guo-api             // 接口模块
│       └── guo-api-system                          // 系统接口
├── guo-common          // 通用模块
│       └── guo-common-core                         // 核心模块
│       └── guo-common-datascope                    // 权限范围
│       └── guo-common-datasource                   // 多数据源
│       └── guo-common-log                          // 日志记录
│       └── guo-common-redis                        // 缓存服务
│       └── guo-common-security                     // 安全模块
│       └── guo-common-swagger                      // 系统接口
├── guo-modules         // 业务模块
│       └── guo-provider                            // 系统模块 [9201]
│       └── guo-consumer                            // 代码生成 [9202]
│       └── guo-swagger                             // 定时任务 [9203]
│       └── guo-sentinel-base                       // 熔断与限流 [9204]
│       └── guo-sentinel-provider                   // 熔断与限流远程服务提供 [9205]
│       └── guo-product                             // 文件服务 [9206]
│       └── guo-account                             // 文件服务 [9207]
├── guo-visual          // 图形化管理模块
│       └── guo-monitor                      // 监控中心 [9100]
├── guo-starter         // 自定义starter
│       └── guo-hello-spring-boot-starter                            // SPI+注解annotation实现自定义stater
│       └── guo-hello-spring-boot-starter-autoconfigurer             // SPI+注解annotation实现自定义stater自动配置
│       └── hello-spring-boot-starter                                // SPI实现自定义stater
│       └── hello-spring-boot-starter-autoconfigurer                 // SPI实现自定义stater自动配置
│       └── guo-spring-boot-starter                                // SPI实现自定义stater
│       └── guo-spring-boot-starter-autoconfigurer                 // SPI实现自定义stater自动配置
│       └── hello-spring-boot-starter-test                           // 自定义stater测试项目 [9208]
├── guo-feign-integration         // feign的集成项目
│       └── guo-feign-gateway                            	// 网关[9301]
│       └── guo-feign-bussiness             			// feign集成项目业务模块
│       │			└── guo-feign-account                  // 账户[9302]
│       │			└── guo-feign-product                  // 产品[9303]
│       │			└── guo-feign-user                     // 用户[9304]
│       └── guo-feign-bussiness-api             			// 业务接口模块
│       │			└── guo-feign-account-api              // 账户
│       │			└── guo-feign-product-api              // 产品
│       │			└── guo-feign-user-api                 // 用户
│       └── guo-feign-common     // feign集成项目公共模块
│       			└── guo-feign-common-core              // 公共核心
├──pom.xml                // 公共依赖
~~~
