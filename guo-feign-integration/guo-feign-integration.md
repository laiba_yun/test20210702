```
├── guo-feign-integration         // feign的集成项目
│       └── guo-feign-gateway                            	// 网关[9301]
│       └── guo-feign-bussiness             			// feign集成项目业务模块
│       │			└── guo-feign-account                  // 账户[9302]
│       │			└── guo-feign-product                  // 产品[9303]
│       │			└── guo-feign-user                     // 用户[9304]
│       └── guo-feign-bussiness-api             			// 业务接口模块
│       │			└── guo-feign-account-api              // 账户
│       │			└── guo-feign-product-api              // 产品
│       │			└── guo-feign-user-api                 // 用户
│       └── guo-feign-common     // feign集成项目公共模块
│       			└── guo-feign-common-core              // 公共核心
```

